import 'react-native-gesture-handler';
import React, { Component } from 'react'
import { Button, Text, View } from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Login from './component/Login';
import Home from './component/Home'
import Detail from './component/Detail';
import Add from './component/Add';

export const baseURL = 'http://110.74.194.124:15011/'
export const header = {
  'Content-Type': 'Application/json',
  'Authorization': 'Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ='
}

const Stack = createStackNavigator();
export default class App extends Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Login">
          <Stack.Screen name="Home" 
            component={Home} 
            options={({ navigation }) => ({
              headerRight: props => <Button 
                title="Add"
                onPress={() => navigation.navigate('Add')}
              />,
            })}
          />
          <Stack.Screen name="Login" component={Login} />
          <Stack.Screen name="Detail" component={Detail} />
          <Stack.Screen name="Add" component={Add} />
        </Stack.Navigator>
      </NavigationContainer>
    )
  }
}


