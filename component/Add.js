import React, { Component } from 'react'
import { Button, Text, View, TextInput, TouchableOpacity, StyleSheet, Dimensions, Image, ActivityIndicator } from 'react-native'
import ImagePicker from 'react-native-image-picker'

import {baseURL} from './../App'
const W = Dimensions.get('screen').width
export default class Add extends Component {
    constructor(props){
        super(props)
        this.state = {
            article: '',
            title: '',
            description: '',
            image: '',
            avatarSource: null,
            isUploading: false,
            status: 1
        }
    }

    uploadArticle = async () => {
        const article = {
            title: this.state.title,
            description: this.state.description,
            image: this.state.avatarSource
        }
        await fetch(`${baseURL}v1/api/articles`, {
            method: 'POST',
            headers: {
                'Content-Type': 'Application/json',
                'Authorization': 'Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ='
            },
            body: JSON.stringify(article)
        })
        .then(res => res.json())
        .then(res => this.setState({
            status: res.code,
            article: res.data
        }))

        if (this.state.status == 2222){
            this.props.navigation.navigate('Home', {
                article: this.state.article
            })
        }

 
    }
    selectImage = async () => {
        ImagePicker.showImagePicker({
            mediaType: 'photo',
            allowsEditing: true
        }, (response) => {
            if (response.didCancel){
                console.log("user did candcel")
            }else if (response.error){
                console.log("response error")
            }else {
                this.setState({
                    isUploading: true
                })
                let file = new FormData();
                file.append('file', { type: response.type, uri: response.uri, name: response.type.substring(4, response.type.length)})
                console.log("file", file)
                fetch('http://110.74.194.124:15011/v1/api/uploadfile/single', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'multipart/form-data',
                        'Authorization': 'Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ='
                    },
                    body: file
                })
                .then(res => res.json())
                .then(res => {
                    this.setState({
                        avatarSource: res.data,
                        isUploading: false
                    })
                })


            }
        })
    }
    render() {
        return (
            <View style={styles.container}>
                <Text> Add New Article </Text>
                <TextInput
                    style={styles.input} 
                    placeholder="Title"
                    onChangeText={(title) => this.setState({title})}
                />
                <TextInput
                    style={styles.input}  
                    placeholder="Descripton"
                    onChangeText={(description) => this.setState({description})}
                />
                {
                    this.state.avatarSource != null && <Image
                        style={{width: W - 50, height: 200}} 
                        source={{uri: this.state.avatarSource}}
                    />
            
                }

                {
                    this.state.isUploading && <ActivityIndicator size="large" />
                }
               
                <Button 
                    title="Select Image"
                    onPress={() => this.selectImage()}
                />
                <TouchableOpacity 
                    onPress={() => this.uploadArticle()}
                    style={styles.btn}>
                    <Text>Add New</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    input: {
        width: W - 40,
        height: 70,
        borderRadius: 15,
        padding: 10,
        borderWidth: 1,
        borderColor: 'gray',
        margin: 10
    }, 
    btn: {
        width: W - 40,
        height: 70,
        backgroundColor: 'lightblue',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 35
    }
})
