import React, { Component } from 'react'
import { Text, View } from 'react-native'
import {baseURL} from './../App'

export default class Detail extends Component {
    constructor(props){
        super(props)
        this.state = {
            article: ''
        }
    }
    fetchOneData = (id) => {
        fetch(`${baseURL}v1/api/articles/${id}`, {
            method: 'GET',
            headers: {
                'Accept': 'Application/json',
                'Authorization': 'Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ='
            }
        })
        .then(res => res.json())
        .then(result => {
            console.log(result.data)
            this.setState({
                article: result.data
            })
        })
    }
    componentDidMount(){
        this.fetchOneData(this.props.route.params.id)
    }
    render() {
        const {article} = this.state
        return (
            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                <Text> You Got {this.props.route.params.id} </Text>
                <Text>{article.title}</Text>
            </View>
        )
    }
}
