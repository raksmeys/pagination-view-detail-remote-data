

import React, { Component } from 'react'
import { Text, View, FlatList, StyleSheet, Dimensions, Image, ActivityIndicator, Button } from 'react-native'
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler'
import { SafeAreaView } from 'react-native-safe-area-context'
import {baseURL, header} from './../App'
import Swipeout from 'react-native-swipeout'

const W = Dimensions.get('screen').width
export default class Home extends Component {

    constructor(props){
        super(props)
        this.state = {
            id: 1,
            page: 1,
            totalPage: 1,
            articles: [],
            isLoading: false,
            refreshing: false,
            title: '',
            isFirstLauch: false
        }
    }

    

    fetchNewData = () => {
        this.setState({page: 1, articles: []}, this.searchArticle)
    } 

    searchArticle = () => {
        this.setState({articles: []})
        fetch(`${baseURL}v1/api/articles?title=${this.state.title}&page=${this.state.page}&limit=15`, {
            method: 'GET',
            headers: header
        })
        .then(res => res.json())
        .then(result => {
            this.setState({
                articles: this.state.articles.concat(result.data),
                totalPage: result.pagination.total_pages,
                isLoading: false
            })
        })
        .catch(error => console.log(error))
    }
    
    fetchData = async () => {
        fetch(`${baseURL}v1/api/articles?page=${this.state.page}&limit=15`, {
            method: 'GET',
            headers: {
                'Accept': 'Application/json',
                'Authorization': 'Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ='
            }
        })
        .then(res => res.json())
        .then(result => {
            console.log(result.data)
            this.setState({
                articles: this.state.articles.concat(result.data),
                totalPage: result.pagination.total_pages
            })
        })
        .catch(error => console.log(error))
    }

    componentDidMount(){
        this.setState({isLoading: true}, this.searchArticle)
    }
    componentDidUpdate(prevProps, prevState){
        console.log("component did update")

        this.props.route.params.article !== undefined && prevState.articles.unshift(this.props.route.params.article)
     
    }


    renderFooter = () => {
        
        return (
            this.state.isLoading ? 

            <View>
                <ActivityIndicator size="large" />
            </View>
            : 
            null
        )
    }

    handleLoadMore = () => {
        this.state.page >= this.state.totalPage ? 
        this.setState({isLoading: false}) : this.setState({page: this.state.page + 1, isLoading: true}, this.searchArticle)

    }
    seletedItem = (id) => {
        console.log('selected', id)
        this.props.navigation.navigate('Detail', {id: id})
    }

    deleteArticle = () => {
        fetch(`${baseURL}v1/api/articles/${this.state.id}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'Application/json',
                'Authorization': 'Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ='
            }
        })
        .then(res => res.json())
        .then(res => {
            const filterArticle = this.state.articles.filter(res => res.id !== this.state.id)
            this.setState({articles: filterArticle, isLoading: true}, this.searchArticle)
        })
    }


    render() {
        
        const {articles} = this.state

        var swipeoutBtns = [
            {
              text: 'Button',
              onPress: () => this.deleteArticle()
            }
          ]
        
        return (

            <SafeAreaView style={{flex: 1, flexDirection: 'column', justifyContent: 'space-evenly'}}>
                
                <View>
                        <TextInput
                            style={styles.searchBarStyle} 
                            placeholder="Search Article"
                            onChangeText={(title) => {
                                this.setState({title: title})
                                this.searchArticle
                            }}
                        />
                </View>
                <View >
                   
                    {
                        articles.length == 0 ? <Text>No Result</Text>
                        : 
                            <FlatList 
                            showsVerticalScrollIndicator={false}
                            data={articles}
                            renderItem={({item, index}) => 
                                <Swipeout 
                                    onOpen={() => this.setState({id: item.id})}
                                    right={swipeoutBtns}>
                                    <TouchableOpacity >
                                        
                                        <Items 
                                        imageSource={item.image_url}
                                        title={item.title}
                                        desc={item.description}
                                        />
                                        <Text>{index}</Text>
                                    
                                    </TouchableOpacity>
                                </Swipeout>
                            }
                            legacyImplementation={true}
                            onEndReachedThreshold={0}
                            refreshing={this.state.refreshing}
                            onRefresh={this.fetchNewData}
                            onEndReached={this.handleLoadMore}
                            ListFooterComponent={this.renderFooter}
                            keyExtractor={(item) => item.id}
                           
                        />
                    }

                 
                    
                </View>
            </SafeAreaView>
           
        )
    }
}

export function Items ({imageSource, title, desc}){
    return (
        <View style={{flex: 1, flexDirection: 'row'}}>
            <Image 
                style={{width: 100, height: 100}}
                source={{uri: imageSource == null ? 'https://images.pexels.com/photos/2236713/pexels-photo-2236713.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500' : imageSource}}
            />
           <View style={{flex: 1, flexDirection: 'column'}}>
                <Text
                    numberOfLines={1}
                    ellipsizeMode="tail" 
                    style={{fontSize: 20, color: 'red', margin: 10}}>{title}</Text>
                <Text 
                    numberOfLines={2}
                    ellipsizeMode="tail"
                    style={{color: 'darkgray', margin: 10}}>{desc}</Text>
           </View>
            
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        margin: 10,
        flex: 1,
        justifyContent: 'center',

    },
    containerCard: {
        marginTop: 5

    },
    searchBarStyle: {
        width: Dimensions.get('screen').width - 50,
        height: 70,
        borderWidth: 1,
        borderColor: 'grey',
        borderRadius: 15,
        
    }
})
