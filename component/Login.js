import React, { useState } from 'react'
import { Text, View, StyleSheet, TextInput, Dimensions, TouchableOpacity } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { baseURL, header } from '../App';

export default Login = ({ navigation }) => {
    const [email, setEmail] = useState('')
    const [pwd, setPwd] = useState('')
    const [error, setError] = useState('')

    const storeData = async (email, pwd) => {
        try {
            await AsyncStorage.setItem('@email', email)
            await AsyncStorage.setItem('@pwd', pwd)
        } catch (e) {
            // saving error
        }
    }

    const getData = async () => {
        try {
            const email = await AsyncStorage.getItem('@email')
            const pwd = await AsyncStorage.getItem('@pwd')
            if (email !== null) {
                // value previously stored
                console.log(email)
            }
        } catch (e) {
            // error reading value
        }
    }

    const handleLogin = async () => {

        const user = {
            email: email,
            password: pwd
        }

        await fetch(`${baseURL}/v1/api/user/authentication`, {
            method: 'POST',
            headers: header,
            body: JSON.stringify(user)
        })
        .then(res => res.json())
        .then(res => {
            if (res.code == 9999){
                setError(res.message)
            }else if (res.code == 2222){
                AsyncStorage.setItem('@email', email)
                AsyncStorage.setItem('@pwd', pwd)
                navigation.navigate('Home')
            }
        })
         
        const mail = await AsyncStorage.getItem('@email')
        console.log(mail)
        // if (username == "") {
        //     setError('*Require')
        // } else {
        //     setError('')
        //     // store data
        //     // get data => navigate to home screen  (data)
        //     await AsyncStorage.setItem('@name', username)
        //     const value = await AsyncStorage.getItem('@name')

        //     console.log(value)

        //     navigation.navigate('Home', {
        //         name: value
        //     })
        // }
   
    }


    return (

        <View style={styles.container}>
    
            <Text style={{color: 'red'}}>{error}</Text>
            <TextInput
                style={styles.inputStyle}
                placeholder="Email"
                keyboardType="email-address"
                onChangeText={(email) => setEmail(email)}
            />
            <TextInput
                style={styles.inputStyle}
                placeholder="Password"
                secureTextEntry={true}
                onChangeText={(pwd) => setPwd(pwd)}
            />
            <TouchableOpacity
                style={styles.btnStyle}
                onPress={() => handleLogin()}
            >
                <Text>
                    Login
                    </Text>
            </TouchableOpacity>

        </View>
    )

}



const WIDTH = Dimensions.get('screen').width
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    inputStyle: {
        width: WIDTH - 50,
        height: 70,
        borderRadius: 15,
        padding: 10,
        borderColor: 'grey',
        borderWidth: 1,
        margin: 10

    },
    btnStyle: {
        width: WIDTH - 50,
        height: 70,
        borderRadius: 35,
        backgroundColor: 'lightblue',
        justifyContent: 'center',
        alignItems: 'center'
    }
})
